from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from .views import *


urlpatterns = [

	url(r'^log-out/$', log_out, name='log-out'),
	url(r'^sign-in/$', sign_in, name='sign_in'),
	# url(r'^d/change-password/$', change_password, name='change-password'),
	url(r'^$', dashboard, name='dashboard'),
	url(r'^work-page-search/a/$', work_page_search, name='work-page-search'),
	url(r'^employee-search-works/a/$', employee_search, name='employee-search-works'),
	url(r'^general-all-user/(?P<u_slug>[\w-]+)/$', general_all_user, name='general-all-user'),
	# url(r'^general-all-user/(?P<pk>[\w-]+)/remove/$', general_all_user_remove, name='general-all-user-remove'),
	url(r'^employee-page/(?P<e_id>[\w-]+)/places/$', employee_page_places, name='employee-page-places'),
	url(r'^employee-page/(?P<e_id>[\w-]+)/$', employee_page, name='employee_page'),
	url(r'^customer-page/(?P<c_id>[\w-]+)/$', customer_page, name='customer-page'),
	url(r'^employee-page-search/(?P<e_id>[\w-]+)/$', employee_page_search, name='employee-page-search'),
	url(r'^employee-location/(?P<e_id>[\w-]+)/(?P<day>[0-9]{2})-(?P<month>[0-9]{2})-(?P<year>[0-9]{4})/$', employee_day_checks, name='employee-day-checks'),
	url(r'^employee-add-to-customer/$', employee_add_to_customer, name='employee-add-to-customer'),
	url(r'^my-works-list/c/$', customer_works_list, name='customer-works-list'),
	url(r'^add-user/$', sign_up, name='sign-up'),
	url(r'^edit-user/(?P<user_type>[\w-]+)/(?P<user_id>[\w-]+)/$', edit_user, name='edit-user'),
	url(r'^remove-user/(?P<user_type>[\w-]+)/(?P<pk>[\w-]+)/$', general_all_user_remove, name='remove-user'),
	url(r'^customer/place/list/$', company_list, name='company-list'),
	url(r'^customer/place/create/$', company_create, name='company-create'),
	url(r'^customer/place/edit/(?P<p_id>[\w-]+)/$', company_edit, name='company-edit'),
	url(r'^customer/place/remove/(?P<p_id>[\w-]+)/$', company_remove, name='company-remove'),

]