from celery import shared_task


@shared_task
def remove_place_task(pk):
    from firebase_admin import firestore
    store = firestore.client()
    data = {'deleted': True}
    print('**** remove-place-task: {0!r}'.format(pk))
    place_ref = store.collection('places').document("{}".format(pk)).set(data, merge=True)



@shared_task
def remove_user(pk,user_type):
    from firebase_admin import firestore, auth
    store = firestore.client()
    data = {'deleted': True}
    print('**** remove-user-task: {0!r}'.format(pk))
    user_ref = store.collection(user_type).document("{}".format(pk)).set(data, merge=True)
    user = auth.update_user(
        pk,
        disabled=True,
    )
    print('______________ remove user start __________________')
    print("--------- user = {} ---------".format(user))
    print("********* user_ref = {} *********".format(user_ref))
    print('______________ remove user end ____________________')