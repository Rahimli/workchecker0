import copy
import json
import uuid
from datetime import timedelta
import firebase_admin
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, Http404, HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate

from django.contrib.auth.views import logout
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.datetime_safe import datetime, date
import datetime as pyt_datetime
import pyrebase
from geoposition import Geoposition

from home.forms import *
import uuid

from django.utils.translation import ugettext as _
# Create your views here.
from home.functions import check_dict_key, split_text
from home.tasks import remove_place_task, remove_user

firebase = pyrebase.initialize_app(settings.FB_CONFIG)
auth_fb = firebase.auth()
database_fb = firebase.database()
storage_fb = firebase.storage()

from firebase_admin import credentials, firestore, auth as fs_auth

from workchecker.settings import BASE_DIR

cred = credentials.Certificate(os.path.join(BASE_DIR, 'static/system/workertracker-ed2f5-firebase-adminsdk-6fw4q-1dbf39de56.json'))
app = firebase_admin.initialize_app(cred)

store = firestore.client()




def base(req=None):
    data = {
        'now':datetime.now(),
        'base_site_name' : settings.SITE_NAME,
    }
    return data

def check_user(req=None):
    return_val = [0,None,None,None]
    session_expiry_date = req.session.get_expiry_date()
    # # print("---------  session_expiry_date = {}".format(session_expiry_date))
    # if not check_valid_session(req):
    #     auth.logout(req)
    try:
        idtoken = req.session['token']
        uid = req.session['uid']
        profile_val = req.session['user_profile']
        user_type = req.session['user_type']
        email = req.session['email']
        if idtoken and uid and profile_val and email:
            profile_val['id'] = uid
            profile_val['email'] = email
            profile_val['user_type'] = user_type
            return_val = [1,idtoken,uid,profile_val]
    except:
        pass
    return return_val

def check_valid_session(request):
    return_val = True
    session_expiry_date = request.session.get_expiry_date()
    now = pyt_datetime.datetime.now()
    # # print("----------------------------------")
    # # print(type(session_expiry_date))
    # # print(type(now))
    # seconds_left = (session_expiry_date - now)
    # # # print("seconds_left == {}".format(seconds_left))
    if session_expiry_date.date() <= now.date():
        return_val = False
    return return_val

def base_auth(req=None):
    #
    # try:
    #     req.session['uid']
    # except:
    #     return HttpResponseRedirect(reverse('home:sign_in'))
    data = {
        'now':datetime.now(),
        'base_site_name' : settings.SITE_NAME,
    }
    user = check_user(req=req)
    # # # print("-----------------------------------------------------------")
    # # # print(user)
    # # # print(user[3])
    # # # print("____________________________________________________________")
    if user[0] == 1:
        # user_profile_val = user[3]
        user_profile = user[3]
        data.update({'base_user_profile':user_profile})
    else:
        data.update({'base_user_profile':None})
        # # # print("data['base_user_profile']  = {}".format(data['base_user_profile']))
    return data



# auth.

def sign_in(request):
    login_form = LoginForm(request.POST or None)

    auth.logout(request)
    next_url = request.GET.get('next_url')
    context = base(req=request)
    context['login_form'] = login_form
    context['next_url'] = next_url
    # return HttpResponse(next_url)
    if request.method == 'POST':
        if login_form.is_valid():
            clean_data = login_form.cleaned_data
            email = clean_data.get('username_or_email')
            password = clean_data.get('password')

            # if remember_me:
            #     remember_me = True
            # else:
            #     remember_me = False
            #     remember_me = False
            try:
                user = auth_fb.sign_in_with_email_and_password(email=email,password=password)
                # return HttpResponse(next_url)
                if next_url == 'None' or not next_url:
                    next_url = reverse('home:dashboard')
                else:
                    pass
                # # print('next_url={}')
                # return HttpResponse(next_url)
                # print("user={}".format(user))
                session_id = user['idToken']
                ui_id = user['localId']
                request.session['uid'] = str(ui_id)
                request.session['token'] = str(session_id)
                log_out_bool = False
                user_profile = None
                user_type = 0
                user_query = store.document('users/{}'.format(ui_id)).get().to_dict()
                # print(user_query)
                if user_query and user_query['type'] and (user_query['type'] == 1 or user_query['type'] == 2):
                    user_type = user_query['type']

                    if user_type == 1:
                        user_profile = store.document('admins/{}'.format(ui_id)).get().to_dict()
                    elif user_type == 2:
                        user_profile = store.document('customers/{}'.format(ui_id)).get().to_dict()

                request.session['user_profile'] = user_profile
                request.session['user_type'] = user_type


                request.session.set_expiry(204800)
                # # print("^^^---^^^ user_profile = {}".format(user_profile))
                if user_profile:
                    request.session['email'] = email
                else:
                    auth.logout(request)
                    return render(request, 'home/general/sign-in.html', context=context)
                return HttpResponseRedirect(next_url)
            except:
                message_login = _("Email or Password is incorrect")
            context['message_login'] = message_login

            # else:

    return render(request, 'home/general/sign-in.html', context=context)




def log_out(request):
    if check_user(req=request)[0]:
        auth.logout(request)
    else:
        auth.logout(request)
    return HttpResponseRedirect(reverse('home:sign_in'))



def dashboard(request):
    now = timezone.now()
    context = base_auth(req=request)
    user_profile = context['base_user_profile']

    # all_employees = store.collection(u'employees').get()
    # i_c = 0
    # for all_employees_item in all_employees:
    #     i_c += 1
    #     # if i_c >2:
    #     #     break
    #     try:
    #         all_employees_item_uid = all_employees_item.id
    #         user = fs_auth.get_user(all_employees_item_uid)
    #         user_ref = store.collection('employees').document("{}".format(all_employees_item_uid)).set({'email':user.email}, merge=True)
    #         # print("user = {}".format(user.email))
    #     except:
    #         pass
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:dashboard'))

    context['work_search_form'] = WorkSearchForm(request.POST or None, )

    # # print("context['base_user_profile']['user_type'] = {}".format(user_profile))

    if user_profile['user_type']:
        work_search_form = WorkSearchForm(request.POST or None,initial={'date':'{}'.format(now.strftime('%d-%m-%Y'))})
        context['work_search_form'] = work_search_form
    else:
        raise Http404
    return render(request, 'home/general/dashboard.html', context=context)



def general_all_user(request,u_slug):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:general-all-user', kwargs={'u_slug':u_slug}))

    if context['base_user_profile']['user_type'] == '1' or context['base_user_profile']['user_type'] == 1:
        pass
    else:
        raise Http404
    # # print("--------------------------------------------")
    if u_slug == 'customers':
        page_title = _('Customers')
    elif u_slug == 'employees':
        page_title = _('Employees')
    else:
        raise Http404
    if request.method == 'POST' and request.is_ajax():
        if u_slug == 'customers':
            all_users = store.collection(u'customers').get()
        else:
            all_users = store.collection(u'employees').get()
        _html = ''
        _result_html = ''
        all_users_item_i = 0
        for all_users_item in all_users:
            all_users_item_val = all_users_item.to_dict()
            if check_dict_key(all_users_item_val, 'deleted'):
                continue
            all_users_item_i += 1
            if u_slug == 'customers':
                _html = "{}{}".format(_html,
                                      render_to_string(
                                          'home/include/user-list/customer-row-item.html',
                                          {
                                              'index':all_users_item_i,
                                              'list_item_id':all_users_item.id,
                                              'list_item':all_users_item_val,
                                          })
                                      )
            elif u_slug == 'employees':
                _html = "{}{}".format(_html,
                                      render_to_string(
                                          'home/include/user-list/employee-row-item.html',
                                          {
                                              'index':all_users_item_i,
                                              'list_item':all_users_item_val,
                                              'list_item_id':all_users_item.id,
                                          })
                                      )
        if u_slug == 'customers':
            _result_html = "{}".format(
                render_to_string(
                    "home/include/user-list/customer-row-table.html",
                    {
                        'html': _html
                    }
                )
            )
        elif u_slug == 'employees':
            _result_html = "{}".format(
                render_to_string(
                    "home/include/user-list/employee-row-table.html",
                    {
                        'html': _html
                    }
                )
            )

        data = {'result':_result_html}
        return JsonResponse(data=data)
    context['page_title'] = page_title
    return render(request, template_name='home/admin/user-list.html', context=context)


def general_all_user_remove(request, user_type, pk):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in') + "?next_url=" + reverse('home:sign-up'))

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404

    type = ''
    if user_type == 'customer':
        type = 'customers'
    elif user_type == 'employee':
        type = 'employees'
    else:
        raise Http404
    doc_ref = store.collection(u'{}'.format(type)).document(u'{}'.format(pk))
    # doc_ref.set(data)
    user_detail = doc_ref.get().to_dict()
    if request.method == 'POST' and request.is_ajax():
        message_code = 0
        if user_detail:
            if check_dict_key(user_detail,'deleted'):
                return JsonResponse(data={'message_code' : message_code,'message':'Already deleted'})
        else:
            return JsonResponse(data={'message_code' : message_code,'message':'Already deleted'})
        message_code = 1
        message = "Succesfully deleted"
        remove_user.delay(pk,type)
        data = {
            'message': message,
            'message_code': message_code,
        }
        return JsonResponse(data=data)
    else:
        raise Http404


def company_list(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    place_search_form = PlaceSearchForm(request.POST or None)

    context['place_search_form'] = place_search_form
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404

    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # # # # print('no POST OR AJAX')
        message_code = 0
        if place_search_form.is_valid():
            message_code = 1
            clean_data = place_search_form.cleaned_data
            c_id = clean_data.get('customer', None)

            if int(context['base_user_profile']['user_type']) == 2:
                c_id = context['base_user_profile']['id']
            message_code = 1
            # # print("--------------------------------------------")
            # # print("****** c_id = {}".format(c_id))
            # # print("--------------------------------------------")
            if c_id:
                if c_id == '-':
                    customer_item = None
                else:
                    customer_item = store.document('customers/{}'.format(c_id))
                # # print("*** customer_item = {}".format(customer_item))
                all_datas = store.collection(u'places').where('customerRef', '==', customer_item).get()
                all_datas_i = 0
                # # # print("------ all_datas = {}".format(all_datas))
                for all_datas_item in all_datas:
                    # # print("---- item.to_dict() = {}".format(all_datas_item.to_dict()))
                    all_datas_item_val = all_datas_item.to_dict()
                    if check_dict_key(all_datas_item_val,'deleted'):
                        continue
                    all_datas_i += 1
                    _result_html = "{}{}".format(_result_html,render_to_string(
                        "home/include/_place-row.html",
                        {
                            # 'list_i':all_datas_i,
                            'base_user_profile':context['base_user_profile'],
                            'customer_id':c_id,
                            'place_id':all_datas_item.id,
                            'name':check_dict_key(all_datas_item_val,'name'),
                            'address':check_dict_key(all_datas_item_val,'address'),
                            'location':check_dict_key(all_datas_item_val,'location'),
                            'customer_name':customer_item.get().to_dict()['name'] if customer_item and customer_item.get().to_dict() else "Administration",
                            'url':'url',
                            # 'delete_url':'url',
                        }
                    ))
                    # # # print(_result_html)
                    # # # print(all_datas_item.val()['name'])
            else:
                all_datas = store.collection(u'places').get()

                # all_datas_ref = store.collection(u'places').reference('customerRef').get()
                all_datas_i = 0
                for all_data_item in all_datas:
                    all_data_item_val = all_data_item.to_dict()
                    if check_dict_key(all_data_item_val,'deleted'):
                        continue
                    all_datas_i += 1
                    # # # print("---- item.to_dict() = {}".format(all_data_item_val['customerRef'].get().to_dict()))
                    # # # print("---- item.to_dict() = {}".format(all_data_item_val['customerRef'].get().id))
                    try:
                        customerRef_item = all_data_item_val['customerRef'].get()
                        customerRef_info = customerRef_item.to_dict()
                    except:
                        customerRef_info = None
                    #     # # print(x)

                    _result_html = "{}{}".format(_result_html,render_to_string(
                        "home/include/_place-row.html",
                        {
                            'base_user_profile':context['base_user_profile'],
                            # 'list_i':all_datas_i,
                            'place_id':all_data_item.id,
                            'name':check_dict_key(all_data_item_val,'name'),
                            'address':check_dict_key(all_data_item_val,'address'),
                            'customer_name':customerRef_info['name'] if customerRef_info else "Administration",
                            'url':'url',
                            # 'delete_url':'url',
                        }

                    ))

            _result_html = "{}".format(render_to_string(
                'home/include/places-work-table.html',
                {
                    'html':_result_html
                }
            ))
        data = {'message_code':message_code,"result_html":_result_html}
        return JsonResponse(data=data)

    return render(request, 'home/admin/place-list.html', context=context)



def company_create(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    import time
    from datetime import datetime
    import pytz

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404

    place_form = PlaceCreateEditForm(request.POST or None)
    now = timezone.now()
    # context['place_form'] = place_form
    # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if place_form.is_valid():
            clean_data = place_form.cleaned_data
            customer = clean_data.get('customer',None)
            name = clean_data.get('name',None)
            address = clean_data.get('address',None)
            position = clean_data.get('position',None)
            # # print("position = {}".format(position[0]))
            # # print("position = {}".format(position[1]))
            if position:
                position_val = firestore.GeoPoint(float(position[0]), float(position[1]))
            else:
                position_val = False
            # position_val = {'latitude':float(position[0]),'longitude':float(position[1])}
            if address:
                pass
            else:
                address = ''
            data = {
                'name':name,
                'address':address,
                'deleted':False,
                'location':position_val
            }
            if customer:
                try:
                    customer_ref = store.document('customers/{}'.format(customer))
                    # if customer_ref
                    data['customerRef'] = customer_ref
                except:
                    data['customerRef'] = None
            else:
                data['customerRef'] = None
            customer_place_ref = store.collection(u'places').add(data)
            # # print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            # # # print(customer_place_ref[1].id)
            # # print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            return HttpResponseRedirect(reverse('home:company-edit',kwargs={'p_id':customer_place_ref[1].id}))
            # success_message = 'Succesfully added'
            # context['success_message'] = success_message
    # context['tours'] = Tour.objects.filter(active=True)
    context['place_form'] = place_form
    return render(request, 'home/admin/place-form.html', context=context)




def company_edit(request, p_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    import time
    from datetime import datetime
    import pytz

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404
    place_obj = store.collection('places').document("{}".format(p_id)).get()
    # # # print("place_objplace_obj = {}".format(place_obj.to_dict()['location'].latitude))
    # # # print("place_objplace_obj = {}".format(place_obj.to_dict()['location'].longitude))
    # # print("place_obj = {}")
    if place_obj.to_dict():

        if place_obj.to_dict()['deleted']:
            raise Http404
        inital_val = {
            'name':place_obj.to_dict()['name'],
            'address':place_obj.to_dict()['address'],

            # 'address':dict(place_obj)['address'],
        }
        location_val = check_dict_key(place_obj.to_dict(),'location')
        # print("**********************************************8")
        # # print(location_val.latitude)
        # print("**********************************************8")
        if location_val:
            inital_val.update({'position': Geoposition(location_val.latitude, location_val.longitude)})
        try:
            if place_obj.to_dict()['customerRef']:
                inital_val['customer'] = place_obj.to_dict()['customerRef'].get().id
        except:
            pass
    else:
        raise Http404
    place_form = PlaceEditForm(request.POST or None,initial=inital_val)
    now = timezone.now()
    # context['place_form'] = place_form
    # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if place_form.is_valid():
            clean_data = place_form.cleaned_data
            clean_data = place_form.cleaned_data
            customer = clean_data.get('customer',None)
            name = clean_data.get('name',None)
            address = clean_data.get('address',None)
            position = clean_data.get('position',None)
            data = {
                'name':name,
                'address':address,
            }
            if position:
                position_val = firestore.GeoPoint(float(position[0]), float(position[1]))
                data.update({'location':position_val})
            if customer:
                try:
                    customer_ref = store.document('customers/{}'.format(customer))
                    # if customer_ref
                    data['customerRef'] = customer_ref
                except:
                    data['customerRef'] = None
            else:
                data['customerRef'] = None
            # except:
            #     data['customerRef'] = ''
            # else:
            #     data['customerRef'] = ''
            # # # print(data['customerRef'].get().to_dict())
            # # print(data)
            place_ref = store.collection('places').document("{}".format(p_id)).set(data, merge=True)



            return HttpResponseRedirect(reverse('home:company-list',kwargs={}))
            # success_message = 'Succesfully added'
    context['place_form'] = place_form
    context['place_obj'] = place_obj
    return render(request, 'home/admin/place-form.html', context=context)




def company_remove(request, p_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404
    place_obj = store.collection('places').document("{}".format(p_id)).get()
    if place_obj.to_dict():
        pass
    else:
        raise Http404
    if request.method == 'POST' and request.is_ajax():
        message_code = 1
        message = "Succesfully ready"
        remove_place_task.delay(p_id)
        data = {
            'message':message,
            'message_code':message_code,
        }
        return JsonResponse(data=data)
    else:
        raise Http404






def sign_up(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:sign-up'))

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404

    sign_up_form = SignupForm(request.POST or None, request.FILES or None)
    now = timezone.now()
    context['sign_up_form'] = sign_up_form
    # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if sign_up_form.is_valid():
            clean_data = sign_up_form.cleaned_data
            name = clean_data.get('name')
            email = clean_data.get('email')
            password = clean_data.get('password')
            code = clean_data.get('code')
            address = clean_data.get('address')
            post_code = clean_data.get('post_code')
            city = clean_data.get('city')
            phone = clean_data.get('phone')
            image = clean_data.get('image')
            document = clean_data.get('document')
            address_document = clean_data.get('address_document')
            contract_file = clean_data.get('contract_file')
            user_type = clean_data.get('user_type')
            user_type = int(user_type)
            try:
                user_fb = auth_fb.create_user_with_email_and_password(email=email,password=password)
            except:
                sign_up_form.add_error('email', _('Email allready use'))
                return render(request, 'home/admin/user-add.html', context=context)
            # idtoken = request.session['uid']
            # a = auth_fb.get_account_info(idtoken)
            # a = a['users']
            # a = a[0]
            # a = a['localId']
            # # # # print("info"+str(a))
            # photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
            # document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
            # address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
            # contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
            # photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['uid'])
            # document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['uid'])
            # address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['uid'])
            # contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['uid'])
            # return HttpResponse(storage_fb.child(photo_file_name).get_url(request.session['uid']))
            # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
            # # # # print(photo_file_name_url)
            # return HttpResponse(photo_file_name_url)
            if code:
                pass
            else:
                code = ' '
            if address:
                pass
            else:
                address = ' '
            if post_code:
                pass
            else:
                post_code = ' '
            if city:
                pass
            else:
                city = ' '
            if phone:
                pass
            else:
                phone = ' '
            data = {
                'name':name,
                'status':1,
                'code':code,
                'address':address,
                'post_code':post_code,
                'city':city,
                'email':email,
                'phone':phone,
                'deleted':False
                # 'image':storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens']),
                # 'document':storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens']),
                # 'address_document':storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens']),
                # 'contract_file':storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens']),
            }
            if image:
                photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
                photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['token'])
                data['image'] = storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens'])
            else:
                data['image'] = ''
            if user_type != 1:
                if document:
                    document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
                    document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['token'])
                    data['document'] = storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens'])
                else:
                    data['document'] = ''
                if address_document:
                    address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
                    address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['token'])
                    data['address_document'] = storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens'])
                else:
                    data['address_document'] = ''
                if contract_file:
                    contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
                    contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['token'])
                    data['contract_file'] = storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens'])
                else:
                    data['contract_file'] = ''
            users_ref = store.collection(u'users').document(u'{}'.format(user_fb['localId']))
            users_ref.set({'type':user_type}, merge=True)
            if user_type == 1:
                doc_ref = store.collection(u'admins').document(u'{}'.format(user_fb['localId']))
                doc_ref.set(data, merge=True)
            elif user_type == 2:
                doc_ref = store.collection(u'customers').document(u'{}'.format(user_fb['localId']))
                doc_ref.set(data, merge=True)
            elif user_type == 3:
                doc_ref = store.collection(u'employees').document(u'{}'.format(user_fb['localId']))
                doc_ref.set(data, merge=True)
            success_message = 'Succesfully added'
            context['success_message'] = success_message
    # context['tours'] = Tour.objects.filter(active=True)
    sign_up_form = SignupForm()
    context['sign_up_form'] = sign_up_form
    return render(request, 'home/admin/user-add.html', context=context)



def edit_user(request,user_type,user_id):
    context = base_auth(req=request)

    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:edit-user', kwargs={'user_type':user_type,'user_id':user_id,}))


    if context['base_user_profile']['user_type'] == 1:
        pass
    else:
        raise Http404
    type = ''
    if user_type == 'customer':
        type = 'customers'
    elif user_type == 'employee':
        type = 'employees'
    else:
        raise Http404
    doc_ref = store.collection(u'{}'.format(type)).document(u'{}'.format(user_id))
    # doc_ref.set(data)
    user_detail = doc_ref.get().to_dict()
    if user_detail:
        if check_dict_key(user_detail,'deleted'):
            raise Http404
    else:
        raise Http404
    # # print("*****  user_detail = {}".format(user_detail))
    # # # # print("user_detail={}".format(user_detail))
    # # # # print("check_dict_key(user_detail,'first_name')={}".format(check_dict_key(user_detail,'first_name')))
    sign_up_form = UserEditForm(request.POST or None, request.FILES or None,
                                       initial={
                                           'name': check_dict_key(user_detail,'name'),
                                           'code': check_dict_key(user_detail,'code'),
                                           'address': check_dict_key(user_detail,'address'),
                                           'post_code': check_dict_key(user_detail,'post_code'),
                                           'city': check_dict_key(user_detail,'city'),
                                           'phone': check_dict_key(user_detail,'phone'),
                                       },)
    now = timezone.now()
    context['sign_up_form'] = sign_up_form
    # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
    if request.method == 'POST':
        if sign_up_form.is_valid():
            clean_data = sign_up_form.cleaned_data
            name = clean_data.get('name')
            # email = clean_data.get('email')
            password = clean_data.get('password')
            code = clean_data.get('code')
            address = clean_data.get('address')
            post_code = clean_data.get('post_code')
            city = clean_data.get('city')
            phone = clean_data.get('phone')
            image = clean_data.get('image')
            document = clean_data.get('document')
            address_document = clean_data.get('address_document')
            contract_file = clean_data.get('contract_file')
            # user_type = clean_data.get('user_type')
            if password:
                user = fs_auth.update_user(
                    user_id,
                    password=password,
                )
            # # # # print(image)
            # return HttpResponse(sign_up_form.is_valid())
            # user_fb = auth_fb.create_user_with_email_and_password(email=email,password=password)
            # idtoken = request.session['token']
            # a = auth_fb.get_account_info(idtoken)
            # a = a['users']
            # a = a[0]
            # a = a['localId']
            # # # # print("info"+str(a))

            data = {
                'name':name,
                'user_type':user_type,
                'status':1,
                'code':code,
                'address':address,
                'post_code':post_code,
                'city':city,
                'phone':phone,
            }
            if image:
                photo_file_name = "users-files/photos/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(image.name)[-1])
                photo_file_name_url = storage_fb.child(format(photo_file_name)).put(image, request.session['token'])
                data['image'] = storage_fb.child(photo_file_name).get_url(photo_file_name_url['downloadTokens'])
            else:
                photo_file_name_url = check_dict_key(user_detail,'image')
                data['image'] = photo_file_name_url
            if document:
                document_file_name = "users-files/documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(document.name)[-1])
                document_file_name_url = document_file_name_url = storage_fb.child(format(document_file_name)).put(document, request.session['token'])
                data['document'] = storage_fb.child(document_file_name).get_url(document_file_name_url['downloadTokens'])
            else:
                document_file_name_url = check_dict_key(user_detail,'document')
                data['document'] = document_file_name_url
            if address_document:
                address_document_file_name = "users-files/address-documents/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(address_document.name)[-1])
                address_document_file_name_url = storage_fb.child(format(address_document_file_name)).put(address_document, request.session['token'])
                data['address_document'] = storage_fb.child(address_document_file_name).get_url(address_document_file_name_url['downloadTokens'])
            else:
                address_document_file_name_url = check_dict_key(user_detail,'address_document')
                data['address_document'] = address_document_file_name_url
            if contract_file:
                contract_file_name = "users-files/contract-files/{}".format(str(uuid.uuid4()) + '-'+ str(uuid.uuid4()) + os.path.splitext(contract_file.name)[-1])
                contract_file_name_url = storage_fb.child(format(contract_file_name)).put(contract_file, request.session['token'])
                data['contract_file'] = storage_fb.child(contract_file_name).get_url(contract_file_name_url['downloadTokens'])
            else:
                contract_file_name_url = check_dict_key(user_detail,'contract_file')
                data['contract_file'] = contract_file_name_url
            # return HttpResponse(storage_fb.child(photo_file_name).get_url(request.session['token']))
            # # # # print("context['base_user_profile']={}".format(context['base_user_profile']))
            # # # # print(photo_file_name_url)
            # return HttpResponse(photo_file_name_url)
            # idtoken = request.session['token']
            doc_ref.set(data, merge=True)
            success_message = 'Succesfully Edited'
            context['success_message'] = success_message
            # sign_up_form = UserEditForm()
    # context['tours'] = Tour.objects.filter(active=True)
    context['sign_up_form'] = sign_up_form
    context['user_type'] = user_type
    return render(request, 'home/admin/user-edit.html', context=context)


def employee_add_to_customer(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-add-to-customer'))

    if int(context['base_user_profile']['user_type']) == 1:
        pass
    else:
        raise Http404
    # return HttpResponse(check_user(req=request))
    # firebase = pyrebase.initialize_app(settings.FB_CONFIG)
    customer_employee_form = CustomerEmployeeForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if customer_employee_form.is_valid():
            clean_data = customer_employee_form.cleaned_data
            place_val = clean_data.get('place')
            employees = clean_data.get('employees')
            start_date = clean_data.get('start_date')
            end_date = clean_data.get('end_date')
            place = store.document('places/{}'.format(place_val))
            if place_val:
                customerRef =  check_dict_key(place.get().to_dict(), 'customerRef')
            else:
                customerRef = None
            start_date_d = start_date
            end_date_d = end_date
            data = {}
            # key = store
            for employees_item in employees:
                data['placeRef'] = place
                data['placeName'] = place.get().to_dict()['name']
                data['start_date'] = start_date_d
                data['end_date'] = end_date_d
                data['customerRef'] = customerRef
                data['employeeRef'] = store.document('employees/{}'.format(employees_item))

                placeEmployee_ref = store.collection(u'placeEmployees').add(data)
                # # print("--- placeEmployee_ref = {}".format(placeEmployee_ref))

            # database_fb.child("customer_employee").child(customer).child(str(start_date.strftime("%d-%m-%Y"))).set(data=data)
            success_message = 'Succesfully added'
            context['success_message'] = success_message
        # else:
        #     return HttpResponse(customer_employee_form.errors)
    customer_employee_form = CustomerEmployeeForm()
    context['customer_employee_form'] = customer_employee_form
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, 'home/admin/employee-add-to-customer.html', context=context)



from collections import OrderedDict


def employee_page(request,e_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee_page', kwargs={'e_id':e_id}))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404
    context['employee_id'] = e_id

    employee_work_search_form = EmployeePageWorkSearchForm(request.POST or None)
    context['employee_work_search_form'] = employee_work_search_form

    user = store.collection(u'{}'.format('employees')).document(u'{}'.format(e_id))
    # doc_ref.set(data)
    # # # # print("mili"+str(millis))
    # user = database_fb.child("users").child(user_id).get()
    user_detail = user.get().to_dict()
    if user_detail:
        if check_dict_key(user_detail,'deleted'):
            raise Http404
    else:
        raise Http404
    # # # # print("******************************************************************")
    context['user_details'] = user_detail
    placeEmployees = store.collection(u'{}'.format('placeEmployees')).where('employeeRef', '==', user).get()
    # print("----------------------------------------------------")
    _html = ''
    for placeEmployees_item in placeEmployees:
        # print(placeEmployees_item.id)
        placeEmployees_item_val = placeEmployees_item.to_dict()
        placeRefVal = check_dict_key(placeEmployees_item_val,'placeRef').get().to_dict()
        placeCustomerRef = check_dict_key(placeRefVal,'customerRef')
        placeCustomerName = 'Administration'
        if placeCustomerRef:
            try:
                placeCustomerName = placeCustomerRef.get().to_dict()['name']
            except:
                pass

        count = 0
        if placeRefVal:
            count += 1
            _html = "{}{}".format(_html,render_to_string(
                'home/include/_place-row-emp.html',
                {
                    'name':check_dict_key(placeRefVal,'name'),
                    'customer_name':placeCustomerName,
                    'count':count,
                    'address':check_dict_key(placeRefVal,'address'),
                }
        ))

    # print("--- _html = {}".format(_html))
    context['html'] = _html
    return render(request, 'home/general/employee.html', context=context)




def employee_page_places(request,e_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee_page', kwargs={'e_id':e_id}))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404
    context['employee_id'] = e_id

    employee_work_search_form = EmployeePageWorkSearchForm(request.POST or None)
    context['employee_work_search_form'] = employee_work_search_form

    user = store.collection(u'{}'.format('employees')).document(u'{}'.format(e_id))
    # doc_ref.set(data)
    # # # # print("mili"+str(millis))
    # user = database_fb.child("users").child(user_id).get()
    user_detail = user.get().to_dict()
    if user_detail:
        if check_dict_key(user_detail,'deleted'):
            raise Http404
    else:
        raise Http404
    # # # # print("******************************************************************")
    context['user_details'] = user_detail
    placeEmployees = store.collection(u'{}'.format('placeEmployees')).where('employeeRef', '==', user).get()
    # print("----------------------------------------------------")
    _html = ''
    for placeEmployees_item in placeEmployees:
        # print(placeEmployees_item.id)
        placeEmployees_item_val = placeEmployees_item.to_dict()
        placeRefVal = check_dict_key(placeEmployees_item_val,'placeRef').get().to_dict()
        placeCustomerRef = check_dict_key(placeRefVal,'customerRef')
        placeCustomerName = 'Administration'
        if placeCustomerRef:
            try:
                placeCustomerName = placeCustomerRef.get().to_dict()['name']
            except:
                pass

        count = 0
        if placeRefVal:
            count += 1
            _html = "{}{}".format(_html,render_to_string(
                'home/include/_place-row-emp.html',
                {
                    'name':check_dict_key(placeRefVal,'name'),
                    'customer_name':placeCustomerName,
                    'count':count,
                    'address':check_dict_key(placeRefVal,'address'),
                }
        ))

    # print("--- _html = {}".format(_html))
    context['html'] = _html
    all_employees_list = []
    all_employees_dict = {}
    _html = ''
    customer_employees_dates_list = []

    # # # # print("all_employees_dict={}".format(all_employees_dict))
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    return render(request, 'home/admin/employee-places.html', context=context)



def employee_day_checks(request,e_id,day,month,year):
    from datetime import timedelta
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-day-checks', kwargs={'e_id':e_id,'day':day,'month':month,'year':year,}))
    try:
        if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
            pass
        else:
            raise Http404
    except:
        raise Http404
    context['employee_id'] = e_id
    date_string = '{}-{}-{}'.format(day,month,year)
    odf = datetime.strptime(str(date_string), '%d-%m-%Y')
    # daily_checkers = daily_checkers.where('checkInDate', ">=", odf)
    checks_location_dayly = store.collection('records/{}/records'.format(e_id))#.where('checkInDate', ">=", odf).where('checkInDate', "<=", odf+timedelta(days=1))

    if int(context['base_user_profile']['user_type']) == 2:
        # # print("_________________________________ int(context['base_user_profile']['user_type']) == 2 == {}".format(
        #     int(context['base_user_profile']['user_type'])))
        customerRef = store.document('customers/{}'.format(context['base_user_profile']['id']))
        checks_location_dayly = checks_location_dayly.where('customerRef', "==", customerRef)
    # checks_hourly_locations = database_fb.child("checker_hourly").child(e_id).child("{}-{}-{}".format(day,month,year)).get()
    user_ref = store.document("employees/{}".format(e_id)).get()
    user_detail = user_ref.to_dict()
    _html_in = ''
    _html_out = ''
    if user_detail:
        if check_dict_key(user_detail,'deleted'):
            raise Http404
    else:
        raise Http404
    # return HttpResponse("user.val() = {}".format(dict(checks_location_dayly.val())))
    # try:
    checks_location_dayly_val = {}
    counter = 0
    result_minute = 0
    for x in checks_location_dayly.get():
        counter += 1
        checks_location_dayly_val_item = x.to_dict()
        checks_location_dayly_val["{}".format(x.id)] = x.to_dict()
        if check_dict_key(checks_location_dayly_val_item,'totalTime'):
            result_minute += check_dict_key(checks_location_dayly_val_item,'totalTime')
        _html_in = "{}{}".format(_html_in,render_to_string(
            'home/include/employee/record-item.html',
            {
                'counter':counter,
                'totalTime':check_dict_key(checks_location_dayly_val_item,'totalTime'),
                'date':check_dict_key(checks_location_dayly_val_item,'checkInDate'),
                'imageUrl':check_dict_key(checks_location_dayly_val_item,'checkInImageUrl'),
                'location':check_dict_key(checks_location_dayly_val_item,'checkInLocation'),
                'checkin': True
            }
        ))
        _html_out = "{}{}".format(_html_out,render_to_string(
            'home/include/employee/record-item.html',
            {
                'counter':counter,
                'totalTime':check_dict_key(checks_location_dayly_val_item,'totalTime'),
                'date':check_dict_key(checks_location_dayly_val_item,'checkOutDate'),
                'imageUrl':check_dict_key(checks_location_dayly_val_item,'checkOutImageUrl'),
                'location':check_dict_key(checks_location_dayly_val_item,'checkOutLocation'),
                'checkin':False

            }
        ))
    context['html_in'] = _html_in
    context['html_out'] = _html_out
    context['result_minute'] = result_minute
    context['user_details'] = user_ref.to_dict()
    context['checks_location_dayly'] = checks_location_dayly_val

        # context['checks_location_dayly_keys'] = context['checks_location_dayly'].keys()
    # except:
    #     raise Http404
    # # # # print("checks_hourly_locations={}".format(checks_hourly_locations))
    # return HttpResponse(checks_hourly_locations)
    # try:
    #     context['checks_hourly_locations'] = dict(checks_hourly_locations.val())
    # except:
    #     context['checks_hourly_locations'] = {}
    # # # # print(now.day)
    context['day'] = day
    context['month'] = month
    context['year'] = year
    return render(request, 'home/general/employee-day-checks.html', context=context)


def customer_works_list(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:customer-works-list'))
    if context['base_user_profile']['user_type'] == 2:
        pass
    else:
        raise Http404
    # try:
    all_data_list_dict = {}
    customerRef = store.document('customers/{}'.format(context['base_user_profile']['id']))
    placeEmployeesRef = store.collection('placeEmployees').where('customerRef' ,'==', customerRef).get()
    _row_html = ''
    list_i = 0
    for placeEmployeesRef_item in placeEmployeesRef:
        list_i += 1
        placeEmployeesRef_item_val = placeEmployeesRef_item.to_dict()
        # # print("--- employee_item_val = {}".format(placeEmployeesRef_item_val['employeeRef'].get().id))
        employee_obj = placeEmployeesRef_item_val['employeeRef'].get()
        _row_html = "{}{}".format(_row_html,render_to_string(
            'home/include/customer/work-list-employee-item.html',
            {
                'list_i':list_i,
                'placeEmployeesRef_item_val':placeEmployeesRef_item_val,
                'employee_item_val':employee_obj.to_dict(),
                'employee_item_id':employee_obj.id,
            }
        ))
    context['row_html'] = _row_html

    return render(request, 'home/customer/my-employees.html', context=context)



def customer_page(request,c_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:customer-page', kwargs={'c_id':c_id}))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2:
        pass
    else:
        raise Http404
    context['customer_id'] = c_id
    # # # # print("******************************************************************")
    # try:
    all_data_list_dict = {}
    customerRef = store.document('customers/{}'.format(c_id))
    context['user_details'] = customerRef.get().to_dict()


    if context['user_details']:
        # print("***_______________________________________________")
        # print(check_dict_key(context['user_details'],'deleted'))
        if check_dict_key(context['user_details'],'deleted'):
            # print("retsjd")
            raise Http404
    else:
        raise Http404

    customer_employee_dates = store.collection('placeEmployees').where('customerRef', '==', customerRef).get()
    _row_html = ''
    list_i = 0
    for customer_employee_date_item in customer_employee_dates:
        list_i += 1
        placeEmployeesRef_item_val = customer_employee_date_item.to_dict()
        employee_obj = placeEmployeesRef_item_val['employeeRef'].get()
        _row_html = "{}{}".format( _row_html,render_to_string(
            'home/include/customer/work-list-employee-item.html',
            {
                'list_i':list_i,
                'placeEmployeesRef_item_val':placeEmployeesRef_item_val,
                'employee_item_val':employee_obj.to_dict(),
                'employee_item_id':employee_obj.id,
            }
        )
        )
    context['row_html'] = _row_html
    # except:
    #     context['all_data_list'] = None
    return render(request, 'home/admin/customer-page.html', context=context)




def employee_search(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    # now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # # # # print("******************************************************************")
    # # # # print(now.day)
    employee_work_search_form = EmployeeWorkSearchForm(context['base_user_profile']['user_type'],context['base_user_profile']['id'],request.POST or None)
    context['employee_work_search_form'] = employee_work_search_form
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # # # # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            message_code = 1
            total_time = 0
            total_time_html = ''
            clean_data = employee_work_search_form.cleaned_data
            e_id = clean_data.get('employee', None)
            order_date_from = clean_data.get('start_date', None)
            order_date_to = clean_data.get('end_date', None)
            try:
                daily_checkers = store.collection('records/{}/records'.format(e_id))
                if int(context['base_user_profile']['user_type']) == 2:
                    customerRef = store.document('customers/{}'.format(context['base_user_profile']['id']))
                    daily_checkers = daily_checkers.where('customerRef', "==", customerRef)
                if order_date_from:
                    odf = datetime.strptime(str(order_date_from), '%d-%m-%Y')
                    daily_checkers = daily_checkers.where('checkInDate', ">=", odf)
                if order_date_to:
                    odf = datetime.strptime(str(order_date_to), '%d-%m-%Y') + timedelta(days=1)
                    # # print("odfodfodfodf = {}".format(odf))
                    daily_checkers = daily_checkers.where('checkInDate', "<=", odf)
            except:
                daily_checkers = None
            all_employees_list = []
            customer_employees_dates_list = []
            if daily_checkers:
                list_i = 0
                for daily_checkers_item in daily_checkers.get():
                    daily_checkers_val_item = daily_checkers_item.to_dict()
                    daily_checkers_val_i += 1
                    append_bool = True
                    if append_bool:
                        list_i += 1
                        try:
                            placeRef_item = daily_checkers_val_item['placeRef'].get()
                            placeRef_info = placeRef_item.to_dict()
                        except:
                            placeRef_info = None
                        totalTime = check_dict_key(daily_checkers_val_item, 'totalTime')
                        if totalTime:
                            total_time += totalTime
                        check_in_date = check_dict_key(daily_checkers_val_item, 'checkInDate')
                        _html = '{0}{1}'.format(_html,render_to_string(
                            'home/include/_employee-work-row.html',
                            {
                                'e_id': e_id,
                                'list_i': list_i,
                                'check_in_date': check_in_date,
                                'check_in_image_url': check_dict_key(daily_checkers_val_item, 'checkInImageUrl'),
                                'check_out_date': check_dict_key(daily_checkers_val_item, 'checkOutDate'),
                                'check_out_image_url': check_dict_key(daily_checkers_val_item, 'checkOutImageUrl'),
                                'totalTime': check_dict_key(daily_checkers_val_item, 'totalTime'),
                                'place_name': placeRef_info['name'] if placeRef_info else 'Adminstration',
                                'date': check_in_date.strftime("%d-%m-%Y"),
                                'url': reverse('home:employee-day-checks',
                                    kwargs={'e_id': e_id, 'day': str(check_in_date.strftime("%d")),
                                            'month': str(check_in_date.strftime("%m")),
                                            'year': str(check_in_date.strftime("%Y")),
                                            }),
                            }))
            total_time_html = "{}".format(render_to_string(
                'home/include/general/-second-to-date.html',
                {
                    'second':total_time
                }
            ))
            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employee-work-table.html',
                {
                    'html': _html,
                }))
            # # # # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,'total_time':total_time_html})
        else:
            pass
            # # # # print('no valid')
            # # # # print(employee_work_search_form.data)
            # # # # print(employee_work_search_form.errors)
    return render(request, 'home/admin/employee-work-search.html', context=context)




def employee_page_search(request,e_id):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # # # # print("******************************************************************")
    # # # # print(now.day)
    employee_work_search_form = EmployeePageWorkSearchForm(request.POST or None)
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    _total_time = 0
    if request.method == 'POST' and request.is_ajax():
        # # # # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            message_code = 1
            clean_data = employee_work_search_form.cleaned_data
            order_date_from = clean_data.get('start_date', None)
            order_date_to = clean_data.get('end_date', None)
            try:
                # daily_checkers = database_fb.child("checker_daily").child(e_id).get()
            # daily_checkers = store.collection(u'records',u'{}'.format(e_id),u'records').document(u'{}'.format(e_id)).collection(u'records')
                daily_checkers = store.collection('records/{}/records'.format(e_id))
                if int(context['base_user_profile']['user_type']) == 2:
                    # # print("_________________________________ int(context['base_user_profile']['user_type']) == 2 == {}".format(int(context['base_user_profile']['user_type'])))
                    customerRef = store.document('customers/{}'.format(context['base_user_profile']['id']))
                    daily_checkers = daily_checkers.where('customerRef', "==", customerRef)
                # # print("**** daily_checkers = {}".format(daily_checkers.get()))
                if order_date_from:
                    odf = datetime.strptime(str(order_date_from), '%d-%m-%Y')
                    daily_checkers = daily_checkers.where('checkInDate', ">=", odf)
                if order_date_to:
                    odf = datetime.strptime(str(order_date_to), '%d-%m-%Y') + timedelta(days=1)
                    # # print("odfodfodfodf = {}".format(odf))
                    daily_checkers = daily_checkers.where('checkInDate', "<=", odf)
            except:
                daily_checkers = None
            if daily_checkers.get():
                list_i = 0
                # # print("^^^ var ")
                # # # print(dict(daily_checkers.get()))
                for daily_checkers_item in daily_checkers.get():

                    daily_checkers_val_item = daily_checkers_item.to_dict()
                    # # print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ = {}".format(daily_checkers_val_item))

                    daily_checkers_val_i += 1
                    # daily_checkers_val_date = daily_checkers_val[0]
                    append_bool = True

                    # # # print("<<<<<<<<<<<<<<<<<<<<<<< append_bool = {} >>>>>>>>>>>>>>>>>>>>>>>>>".format(append_bool))
                    if append_bool:
                        list_i += 1
                        try:
                            placeRef_item = daily_checkers_val_item['placeRef'].get()
                            placeRef_info = placeRef_item.to_dict()
                        except:
                            placeRef_info = None
                        if check_dict_key(daily_checkers_val_item,'totalTime'):
                            _total_time += check_dict_key(daily_checkers_val_item,'totalTime')
                        check_in_date = check_dict_key(daily_checkers_val_item, 'checkInDate')
                        _html = '{0}{1}'.format(_html,render_to_string(
                            'home/include/_employee-work-row.html',
                            {
                                'e_id': e_id,
                                'list_i': list_i,
                                'totalTime':  check_dict_key(daily_checkers_val_item,'totalTime'),
                                'check_in_date':  check_dict_key(daily_checkers_val_item,'checkInDate'),
                                'check_in_image_url': check_dict_key(daily_checkers_val_item,'checkInImageUrl'),
                                'check_out_date': check_dict_key(daily_checkers_val_item,'checkOutDate'),
                                'check_out_image_url': check_dict_key(daily_checkers_val_item,'checkOutImageUrl'),
                                'place_name': placeRef_info['name'] if placeRef_info else 'Adminstration',
                                'date': check_in_date.strftime("%d-%m-%Y"),
                                # 'date': daily_checkers_val_item[0],
                                'url': reverse('home:employee-day-checks',
                                    kwargs={'e_id': e_id, 'day': str(check_in_date.strftime("%d")),
                                            'month': str(check_in_date.strftime("%m")),
                                            'year': str(check_in_date.strftime("%Y")),
                                            }),
                            }))

            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employee-work-table.html',
                {
                    'html': _html,
                }))
            # _result_html = _result_html.replace('<script src="/static/main/assets/global/scripts/datatable.js" type="text/javascript"></script>','')
            # _result_html = _result_html.replace('<script src="/static/main/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>','')
            # _result_html = _result_html.replace('<script src="/static/main/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>','')
            # _result_html = _result_html.replace('<script src="/static/main/assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>','')
            _total_time_html = render_to_string('home/include/general/-second-to-date.html',{'second':_total_time})
            # # # # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,'total_time':_total_time_html})
        else:
            raise Http404
            # # # # print('no valid')
            # # # # print(employee_work_search_form.data)
            # # # # print(employee_work_search_form.errors)
    else:
        raise Http404





def work_page_search(request):
    context = base_auth(req=request)
    if context['base_user_profile'] is None:
        return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
    now = timezone.now()
    if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
        pass
    else:
        raise Http404
    # # print("******************************************************************")
    # # # # print(now.day)
    employee_work_search_form = WorkSearchForm(request.POST or None)
    daily_checkers_val_list = []
    daily_checkers_val_i = 0
    _html = ''
    _result_html = ''
    message_code = 0
    if request.method == 'POST' and request.is_ajax():
        # # # # print('no POST OR AJAX')
        if employee_work_search_form.is_valid():
            total_minute = 0
            message_code = 1
            clean_data = employee_work_search_form.cleaned_data
            customer = clean_data.get('customer', None)
            date_f = clean_data.get('date', None)

            if int(context['base_user_profile']['user_type']) == 2:
                customer = context['base_user_profile']['id']
                customer_obj = store.document('customers/{}'.format(customer))
                # customer_id =
                # customer_obj = store.document('customers/{}'.format(customer))
            elif customer:
                customer_obj = store.document('customers/{}'.format(customer))
            else:
                customer_obj = None
            # if date_f:
            #     pass
            # else:
            #     date_f = '{}-{}-{}'.format(now.day, now.month, now.year)
            all_employees_dict = {}
            # for all_users_item in all_users.each():
            #     # # # print(all_users_item.val()['user_type'])
            #     if int(all_users_item.val()['user_type']) == 3:
            #         all_employees_dict[all_users_item.key()] = {"first_name":check_dict_key(all_users_item.val(), 'first_name'),"last_name":check_dict_key(all_users_item.val(), 'last_name')}

            daily_checkers_dict = {}
            # try:
            employeesRef = store.collection('employees').get()
            # daily_checkers = store.collection('records').get()
            all_record_count = 0
            for employeesRef_item in employeesRef:
                employeesRef_item_val = employeesRef_item.to_dict()
                recordRef = store.collection('records').document("{}".format(employeesRef_item.id)).collection('records')
                # # print("---- **** - employeesRef_item.id = {}".format(employeesRef_item.id))



                if date_f:
                    # # print("^^^^^ date_f = {}".format(type(date_f)))
                    # # print("^^^^^ date_f = {}".format(date_f))
                    odf = datetime.strptime(date_f, '%d-%m-%Y')
                    odt = odf + timedelta(days=1)
                    # # print("##### odf = {}".format(odf))
                    # # print("##### odt = {}".format(odt))
                    recordRef = recordRef.where('checkInDate', ">=", odf).where('checkInDate', "<=", odt)
                    # # print("---------------------------------------------------------")
                    # # print('daily_checkers_dict = {}'.format(len(daily_checkers_dict)))
                    # # print("---------------------------------------------------------")
                # if customer_obj:
                #     # # print("~~~~~~~~~~~~~~~~~~ customer_obj = {}".format(customer_obj.get().to_dict()))
                #     recordRef = recordRef.where('customerRef', '==', customer_obj)

                list_i = 0
                # # print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
                # # print(recordRef)
                # # print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
                for recordRef_item in recordRef.get():
                    daily_checkers_dict_val = recordRef_item.to_dict()
                    try:
                        if customer_obj:
                            if daily_checkers_dict_val['customerRef'].get().id == customer:
                                pass
                            else:
                                continue
                        else:
                            pass
                        try:
                            placeRef_item = daily_checkers_dict_val['placeRef'].get()
                            placeRef_info = placeRef_item.to_dict()
                        except:
                            placeRef_info = None
                        check_in_date = check_dict_key(daily_checkers_dict_val,'checkInDate')
                        totalTime_item = check_dict_key(daily_checkers_dict_val, 'totalTime')
                        if check_in_date:
                            pass
                        else:
                            pass
                            # print("***** check_in_date = {}".format(check_in_date))
                        # print("---- check_in_date = {}".format(check_in_date))

                        if totalTime_item:
                            total_minute += totalTime_item
                        _html = '{0}{1}'.format(_html,render_to_string(
                            'home/include/_employees-work-row.html',
                            {
                                'list_i': list_i,
                                'name':  check_dict_key(employeesRef_item_val,'name'),
                                'check_in_date': check_in_date,
                                'check_in_image_url': check_dict_key(daily_checkers_dict_val,'checkInImageUrl'),
                                'check_out_date': check_dict_key(daily_checkers_dict_val,'checkOutDate'),
                                'check_out_image_url': check_dict_key(daily_checkers_dict_val,'checkOutImageUrl'),
                                'totalTime': totalTime_item,
                                # 'date': type(check_dict_key(daily_checkers_dict_val,'checkInDate')),
                                'place_name': placeRef_info['name'] if placeRef_info else '-',
                                'url': reverse('home:employee-day-checks',
                                    kwargs={'e_id': employeesRef_item.id, 'day': str(check_in_date.strftime("%d")),
                                            'month': str(check_in_date.strftime("%m")),
                                            'year': str(check_in_date.strftime("%Y")),
                                            }),
                            }))
                                    # except:
                                    #     pass
                    except:
                        pass
            _result_html = '{0}{1}'.format(_result_html, render_to_string(
                'home/include/employees-work-table.html',
                {
                    'html': _html,
                }))
            total_minute_html = "{}".format(render_to_string(
                "home/include/general/-second-to-date.html",
                {
                    'second':total_minute,
                }
            ))
            # # # # print("json context['daily_checkers']={}".format(context['daily_checkers']))
            return JsonResponse(data={'message_code':message_code,'html':_result_html,'total_time':total_minute_html})
        else:
            pass
            # # # # print('no valid')
            # # # # print(employee_work_search_form.data)
            # # # # print(employee_work_search_form.errors)

#
#
#
# def work_page_search(request):
#     context = base_auth(req=request)
#     if context['base_user_profile'] is None:
#         return HttpResponseRedirect(reverse('home:sign_in')+"?next_url="+reverse('home:employee-search-works'))
#     now = timezone.now()
#     if int(context['base_user_profile']['user_type']) == 1 or int(context['base_user_profile']['user_type']) == 2 :
#         pass
#     else:
#         raise Http404
#     # # print("******************************************************************")
#     # # # # print(now.day)
#     employee_work_search_form = WorkSearchForm(request.POST or None)
#     daily_checkers_val_list = []
#     daily_checkers_val_i = 0
#     _html = ''
#     _result_html = ''
#     message_code = 0
#     if request.method == 'POST' and request.is_ajax():
#         # # # # print('no POST OR AJAX')
#         if employee_work_search_form.is_valid():
#             message_code = 1
#             clean_data = employee_work_search_form.cleaned_data
#             customer = clean_data.get('customer', None)
#             date_f = clean_data.get('date', None)
#             # if date_f:
#             #     pass
#             # else:
#             #     date_f = '{}-{}-{}'.format(now.day, now.month, now.year)
#             all_employees_dict = {}
#             # for all_users_item in all_users.each():
#             #     # # # print(all_users_item.val()['user_type'])
#             #     if int(all_users_item.val()['user_type']) == 3:
#             #         all_employees_dict[all_users_item.key()] = {"first_name":check_dict_key(all_users_item.val(), 'first_name'),"last_name":check_dict_key(all_users_item.val(), 'last_name')}
#
#             daily_checkers_dict = {}
#             # try:
#             daily_checkers = store.collection('records').get()
#             all_record_count = 0
#             # # print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
#             # # print("%%%%% daily_checkers = {}".format(daily_checkers))
#
#             for x in daily_checkers:
#                 # # print("---- **** - x = {}".format(x.id))
#                 record = store.collection('records').document("{}".format(x.id)).collection('records')
#                 # # print("---- **** - x = {}".format(record))
#                 if date_f:
#                     # # print("^^^^^ date_f = {}".format(type(date_f)))
#                     # # print("^^^^^ date_f = {}".format(date_f))
#                     odf = datetime.strptime(date_f, '%d-%m-%Y')
#                     odt = odf + timedelta(days=1)
#                     # # print("##### odf = {}".format(odf))
#                     # # print("##### odt = {}".format(odt))
#                     record = record.where('checkInDate', ">=", odf).where('checkInDate', "<=", odt)
#                 for x_item in record.get():
#                     all_record_count += 1
#                     daily_checkers_dict['item-{}'.format(all_record_count)] = {
#                         'id': x_item.id,
#                         'content': x_item.to_dict()
#                     }
#                     # # print("*** !!! x_item = {}".format(x_item.to_dict()))
#                 # # print("---- **** - x = {}".format(record))
#             # except:
#             #     daily_checkers = None
#             all_employees_list = []
#             customer_employees_dates_list = []
#             customer_employees_dates_dict = {}
#             # # print("---------------------------------------------------------")
#             # # print('daily_checkers_dict = {}'.format(len(daily_checkers_dict)))
#             # # print("---------------------------------------------------------")
#             # for x in daily_checkers:
#             #     # # print("---- **** - x = {}".format(x.id))
#             #     record = store.collection('records').document("{}".format(x.id)).collection('records')
#             #     # # print("---- **** - x = {}".format(record))
#             #     if date_f:
#             #         # # print("^^^^^ date_f = {}".format(type(date_f)))
#             #         # # print("^^^^^ date_f = {}".format(date_f))
#             #         odf = datetime.strptime(date_f, '%d-%m-%Y')
#             #         odt = odf + timedelta(days=1)
#             #         # # print("##### odf = {}".format(odf))
#             #         # # print("##### odt = {}".format(odt))
#             #         record = record.where('checkInDate', ">=", odf).where('checkInDate', "<=", odt)
#             #     for x_item in record.get():
#             #         all_record_count += 1
#             #         daily_checkers_dict['item-{}'.format(all_record_count)] = {
#             #             'id': x_item.id,
#             #             'content': x_item.to_dict()
#             #         }
#             #         # # print("*** !!! x_item = {}".format(x_item.to_dict()))
#             #     # # print("---- **** - x = {}".format(record))
#             # # except:
#             # #     daily_checkers = None
#             # all_employees_list = []
#             # customer_employees_dates_list = []
#             # customer_employees_dates_dict = {}
#             # # # print("---------------------------------------------------------")
#             # # # print('daily_checkers_dict = {}'.format(len(daily_checkers_dict)))
#             # # # print("---------------------------------------------------------")
#             if len(daily_checkers_dict):
#                 list_i = 0
#                 for index_num in range(0,len(daily_checkers_dict)):
#                     daily_checkers_dict_val = copy.deepcopy(daily_checkers_dict["item-{}".format(index_num+1)])
#                     append_bool = True
#
#                     if append_bool:
#                         _html = '{0}{1}'.format(_html,render_to_string(
#                             'home/include/_employees-work-row.html',
#                             {
#                                 # 'e_id': e_id,
#                                 'list_i': list_i,
#                                 # 'first_name':  all_employees_dict[e_id]['first_name'],
#                                 # 'last_name':  all_employees_dict[e_id]['last_name'],
#                                 'check_in_date':  check_dict_key(daily_checkers_dict_val['content'],'checkInDate'),
#                                 'check_in_image_url': check_dict_key(daily_checkers_dict_val['content'],'checkInImageUrl'),
#                                 'check_out_date': check_dict_key(daily_checkers_dict_val['content'],'checkOutDate'),
#                                 'check_out_image_url': check_dict_key(daily_checkers_dict_val['content'],'checkOutImageUrl'),
#                                 'date': type(check_dict_key(daily_checkers_dict_val['content'],'checkInDate')),
#                                 'place_name':  check_dict_key(daily_checkers_dict_val['content'],'place_name'),
#                                 # 'url': reverse('home:employee-day-checks',
#                                 #     kwargs={'e_id': e_id, 'day': str(daily_checkers_key_item).split('-')[0],
#                                 #             'month': str(daily_checkers_key_item).split('-')[1],
#                                 #             'year': str(daily_checkers_key_item).split('-')[2], }),
#                             }))
#                                 # except:
#                                 #     pass
#
#             _result_html = '{0}{1}'.format(_result_html, render_to_string(
#                 'home/include/employees-work-table.html',
#                 {
#                     'html': _html,
#                 }))
#             # # # # print("json context['daily_checkers']={}".format(context['daily_checkers']))
#             return JsonResponse(data={'message_code':message_code,'html':_result_html,})
#         else:
#             pass
#             # # # # print('no valid')
#             # # # # print(employee_work_search_form.data)
#             # # # # print(employee_work_search_form.errors)
#
