import os

from django import forms
from django.utils.translation import ugettext as _

import pyrebase
from django.conf import settings
from firebase_admin import firestore
from geoposition.forms import GeopositionField

from home.functions import check_dict_key

firebase = pyrebase.initialize_app(settings.FB_CONFIG)
auth_fb = firebase.auth()
database_fb = firebase.database()

# from .views import store



user_type_choice = (
    ('','User Type'),
    (1,'Admin'),
    (2,'Customer'),
    (3,'Employee'),
)

class LoginForm(forms.Form):
    username_or_email = forms.EmailField(max_length=255,min_length=2,required=True,widget=forms.EmailInput(attrs={'placeholder':_('Email'),'autocomplete':'off','class': 'form-control form-control-solid placeholder-no-fix',}))
    password = forms.CharField(max_length=255,required=True,widget=forms.PasswordInput(attrs={'placeholder':_('Password'),'autocomplete':'off','class': 'form-control form-control-solid placeholder-no-fix',}))
    remember_me = forms.BooleanField(required=False)

class SignupForm(forms.Form):
    name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    email = forms.EmailField(max_length=255,required=True,widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control',}))
    password = forms.CharField(max_length=255,min_length=6,required=True,widget=forms.PasswordInput(attrs={'autocomplete':'off','class': 'form-control',}))
    code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    post_code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    city = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    phone = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    image = forms.ImageField(required=False)
    document = forms.FileField(required=False)
    address_document = forms.FileField(required=False)
    contract_file = forms.FileField(required=False)
    user_type = forms.ChoiceField(required=True,choices=user_type_choice,widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control',}))


class UserEditForm(forms.Form):
    name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    code = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    password = forms.CharField(max_length=255,min_length=6,required=False,widget=forms.PasswordInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    post_code = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    city = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    phone = forms.CharField(max_length=255,min_length=2,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    image = forms.ImageField(required=False)
    document = forms.FileField(required=False)
    address_document = forms.FileField(required=False)
    contract_file = forms.FileField(required=False)
    # user_type = forms.ChoiceField(required=True,choices=user_type_choice,widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control',}))

class CustomerEmployeeForm(forms.Form):
    place = forms.ChoiceField(required=True,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2',}))
    employees = forms.MultipleChoiceField(required=True,choices=[],widget=forms.SelectMultiple(attrs={'autocomplete':'off','class': 'form-control select2-multiple'}))
    start_date = forms.DateTimeField(required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':10,'readonly':'',}))
    end_date = forms.DateTimeField(required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,'readonly':'',}))
    def __init__(self,*args, **kwargs):
        super(CustomerEmployeeForm, self).__init__(*args, **kwargs)
        # all_customers_list = []
        all_places_list = []
        all_employees_list = []
        store = firestore.client()
        all_places = store.collection(u'places').get()
        # all_customers = store.collection(u'customers').get()
        all_employees = store.collection(u'employees').get()
        # all_employees_list = []
        for all_places_item in all_places:
            all_places_item_val = all_places_item.to_dict()
            try:
                customer_item = check_dict_key(all_places_item_val, 'customerRef').get().to_dict()
            except:
                customer_item = None

            if check_dict_key(all_places_item_val, 'deleted'):
                continue
            all_places_list.append([all_places_item.id,"{}  --  {}".format(customer_item['name'] if customer_item else 'Adminstration',check_dict_key(all_places_item_val,'name'))])
        #
        # for all_customers_item in all_customers:
        #     all_customers_item_val = all_customers_item.to_dict()
        #     all_customers_list.append([all_customers_item.id,"{}".format(check_dict_key(all_customers_item_val,'name'))])

        for all_employees_item in all_employees:
            all_employees_item_val = all_employees_item.to_dict()
            if check_dict_key(all_employees_item_val, 'deleted'):
                continue
            all_employees_list.append([all_employees_item.id,"{}".format(check_dict_key(all_employees_item_val,'name'))])

        self.fields['place'].choices = [['',_('Choose Place')]] + all_places_list
        self.fields['employees'].choices = [['',_('Choose Employee')]] + all_employees_list

class EmployeePageWorkSearchForm(forms.Form):
    start_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'From'}))
    end_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control','placeholder':'To',}))
    def __init__(self,*args, **kwargs):
        super(EmployeePageWorkSearchForm, self).__init__(*args, **kwargs)


class EmployeeWorkSearchForm(forms.Form):
    employee = forms.ChoiceField(required=True,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Employee'}))
    start_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'From'}))
    end_date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control','placeholder':'To',}))
    def __init__(self,user_type,user_id,*args, **kwargs):
        super(EmployeeWorkSearchForm, self).__init__(*args, **kwargs)
        store = firestore.client()
        all_users = store.collection(u'employees').get()
        all_employees_list = []
        try:
            for all_users_item in all_users:
                all_users_item_val = all_users_item.to_dict()
                if check_dict_key(all_users_item_val, 'deleted'):
                    continue
                all_employees_list.append([all_users_item.id,"{}".format(all_users_item_val['name'])])

                    # # print("all_users_item.val() = {} ".format(all_users_item.val()))
        except:
            pass
        self.fields['employee'].choices = all_employees_list



class WorkSearchForm(forms.Form):
    customer = forms.ChoiceField(required=False,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Customer',}))
    date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'Date'}))
    def __init__(self,*args, **kwargs):
        super(WorkSearchForm, self).__init__(*args, **kwargs)
        store = firestore.client()
        all_users = store.collection(u'customers').get()
        all_customers_list = []
        for all_users_item in all_users:
            all_users_item_val = all_users_item.to_dict()
            if check_dict_key(all_users_item_val, 'deleted'):
                continue
            all_customers_list.append([all_users_item.id,"{}".format(check_dict_key(all_users_item_val,'name'))])
        self.fields['customer'].choices = [['',_('Choose Customer')]] + all_customers_list
        #


class PlaceSearchForm(forms.Form):
    customer = forms.ChoiceField(required=False,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Customer',}))
    # date = forms.CharField(required=False,widget=forms.DateInput(attrs={'autocomplete':'off','class': 'form-control ','placeholder':'Date'}))
    def __init__(self,*args, **kwargs):
        super(PlaceSearchForm, self).__init__(*args, **kwargs)
        store = firestore.client()
        all_users = store.collection(u'customers').get()
        all_customers_list = []
        for all_users_item in all_users:
            all_users_item_val = all_users_item.to_dict()
            if check_dict_key(all_users_item_val, 'deleted'):
                continue
            all_customers_list.append([all_users_item.id,"{}".format(check_dict_key(all_users_item_val,'name'))])

        self.fields['customer'].choices = [['',_('Choose Customer')],['-',_('Administration')]] + all_customers_list


class PlaceCreateEditForm(forms.Form):
    name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,required=False,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    customer = forms.ChoiceField(required=False,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Customer',}))
    position = GeopositionField(required=False)

    def __init__(self,*args, **kwargs):
        super(PlaceCreateEditForm, self).__init__(*args, **kwargs)
        store = firestore.client()
        all_users = store.collection(u'customers').get()
        all_customers_list = []
        for all_users_item in all_users:
            all_users_item_val = all_users_item.to_dict()
            if check_dict_key(all_users_item_val, 'deleted'):
                continue
            all_customers_list.append([all_users_item.id,"{}".format(check_dict_key(all_users_item_val,'name'))])
        self.fields['customer'].choices = [['',_('Choose Customer')],['',_('Administration')]] + all_customers_list


class PlaceEditForm(forms.Form):
    name = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    address = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control',}))
    customer = forms.ChoiceField(required=False,choices=[],widget=forms.Select(attrs={'autocomplete':'off','class': 'form-control select2','placeholder':'Customer',}))
    position = GeopositionField(required=False)

    def __init__(self,*args, **kwargs):
        super(PlaceEditForm, self).__init__(*args, **kwargs)
        store = firestore.client()
        all_users = store.collection(u'customers').get()
        all_customers_list = []
        for all_users_item in all_users:
            all_users_item_val = all_users_item.to_dict()
            if check_dict_key(all_users_item_val, 'deleted'):
                continue
            all_customers_list.append([all_users_item.id,"{}".format(check_dict_key(all_users_item_val,'name'))])
        self.fields['customer'].choices = [['',_('Choose Customer')],['',_('Administration')]] + all_customers_list
