/**
 * Created by macos on 2/1/18.
 */

    var ajax_loader_url = '/static/theme/assets/custom/images/loading-gif-transparent-10.gif';
    $( document ).ready(function() {
        employeePlansShow();
    });
    var work_plan_container = $('#work-plan-container');
    var show_map_link = $('#show-map-button');
    var ajax_employee_map_div = $('#ajax-employee-map-div');
    var url = work_plan_container.attr( "data-ajax-url" );
    var show_map_link_url = show_map_link.attr( "data-ajax-url" );
    var get_change_customers_url = '';
    var get_customer_position_change_url = '';
    var change_position_location_button = $('#change-position-location-button');
    var change_position_location_button_url = change_position_location_button.attr( "data-ajax-url" );
    function employeePlansShow() {
        work_plan_container.html("<div class='text-center'><img style='margin: 10px auto;' src='" + ajax_loader_url + "'/></div>");
        $.ajax({
            dataType: "json",
            type: "GET",
            url: url,
            data: {},
            success: function (data) {
                var message_code = data['message_code'];
                var _html = data['_html'];
                if (message_code == 1){
                    work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }
    var change_plan_week_action_title = $('#change-plan-week-action-title');
    var change_plan_week_action_button = $('#change-plan-week-action-button');
    var change_plan_week_action_form = $('#change-plan-week-action-form');
    function onActionWeek($url,$id,$operation,$title) {
        change_plan_week_action_button.attr('onclick','submitActionPlanEdit(\''+$url+'\')');
        change_plan_week_action_title.html($title + ' - ' + $operation);
        change_plan_week_action_button.show();
    }


    function submitActionPlanEdit($url) {
        work_plan_container.html("<div class='text-center'><img style='margin: 10px auto;' src='" + ajax_loader_url + "'/></div>");
        $.post($url, change_plan_week_action_form.serialize())
            .done(function (data) {
                // data_list_main_content.html(data['result'])
                $("#action-operation-modal").modal('hide');
                employeePlansShow();
            });
    }
    



    function generatePlanPdf($url,$csrf) {
        var main_print_div = $('#main-print-div');
        var admin_pdf_plan_generate_loader = $('#admin-pdf-plan-generate-loader');
        var admin_pdf_plan_generate_button = $('#admin-pdf-plan-generate-button');
        var main_print_content_div = $('#main-print-content-div');
        admin_pdf_plan_generate_button.hide();
        admin_pdf_plan_generate_loader.show();
        $.post($url, {'csrfmiddlewaretoken':$csrf})
            .done(function (data) {
                // console.log(data);
                // data_list_main_content.html(data['result'])
                // $("#action-operation-modal").modal('hide');
                // employeePlansShow();
                main_print_content_div.html(data['_html']);
                admin_pdf_plan_generate_loader.hide();
                admin_pdf_plan_generate_button.show();
                printData('main-print-div');
            });
    }



    function printData($id){
      var divToPrint=document.getElementById($id);

      var newWin=window.open('','Print-Window');

      newWin.document.open();

      newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

      newWin.document.close();

      setTimeout(function(){newWin.close();},10);
    }


    function showPlanMap($url,$list) {
        // alert($url);
        $.ajax({
            dataType: "json",
            traditional: true,
            type: "GET",
            url: $url,
            data: {location_list:$list},
            success: function (data) {
                var message_code = data['message_code'];
                var _html = data['_html'];
                if (message_code == 1){
                    console.log(_html);
                    ajax_employee_map_div.html(_html);
                    $("#todo-task-modal").modal();
                    // ajax_employee_map_div.append(_html);
                    // work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }
    var employees_select = $('#edit-location-order-modal-employee-select');
    var employees_work_day_select = $('#edit-location-order-modal-employee-workday-select');
    var employees_work_week_select = $('#edit-location-order-modal-employee-week-select');
    var location_order_modal_employee_change_location_select = $('#edit-location-order-modal-employee-change-location-select');
    function showOtherPlans($url,$employee_plan_id,$l_id,$get_customers_url,$get_customer_position_change_url) {
        // alert($url);
        this.get_change_customers_url = $get_customers_url;
        get_customer_position_change_url = $get_customer_position_change_url;
        change_position_location_button.attr( "data-ajax-url",$get_customer_position_change_url );
        change_position_location_button_url = $get_customer_position_change_url;
        $.ajax({
            dataType: "json",
            traditional: true,
            type: "GET",
            url: $url,
            data: {employee_plan_id:$employee_plan_id,l_id:$l_id},
            success: function (data) {
                var message_code = data['message_code'];
                var _html_employees_select = data['_html_employees_select'];
                var _html_employees_work_day_select = data['_html_employees_work_day_select'];
                if (message_code == 1){
                    // console.log(_html);
                    employees_select.html('');
                    employees_select.append(_html_employees_select);
                    employees_work_day_select.html('');
                    employees_work_day_select.append(_html_employees_work_day_select);
                    location_order_modal_employee_change_location_select.html('');

                    // $("#todo-task-modal").modal('hide');
                    // ajax_employee_map_div.append(_html);
                    // work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }
    // function showOtherPlans($url,$employee_plan_id,$l_id) {
    //     // alert($url);
    //     $.ajax({
    //         dataType: "json",
    //         traditional: true,
    //         type: "GET",
    //         url: $url,
    //         data: {employee_plan_id:$employee_plan_id,l_id:$l_id},
    //         success: function (data) {
    //             var message_code = data['message_code'];
    //             var _html_employees_select = data['_html_employees_select'];
    //             var _html_employees_work_day_select = data['_html_employees_work_day_select'];
    //             if (message_code == 1){
    //                 // console.log(_html);
    //                 employees_select.append(_html_employees_select);
    //                 employees_work_day_select.append(_html_employees_work_day_select);
    //                 // $("#todo-task-modal").modal('hide');
    //                 // ajax_employee_map_div.append(_html);
    //                 // work_plan_container.html(_html)
    //             }
    //         },
    //         error: function (data) {
    //             // work_plan_container.removeAttr('disabled');
    //         }
    //     });
    // }


    employees_select.change(function(){
        change_position_location_button.hide();
        if (employees_select.val() == '' || employees_work_day_select.val() == '' || employees_work_week_select.val() == ''){
            location_order_modal_employee_change_location_select.html();
        }else {
            console.log(get_change_customers_url);
            location_order_modal_employee_change_location_select.val('');
            setTimeout(function(){
                getPosibleResultsLocations(get_change_customers_url);
            }, 300);
            setTimeout(function(){
                change_position_location_button.show();
            }, 400);
        }
    });


    employees_work_week_select.change(function(){
        change_position_location_button.hide();
        if (employees_select.val() == '' || employees_work_day_select.val() == '' || employees_work_week_select.val() == ''){
            location_order_modal_employee_change_location_select.html();
        }else {
            console.log(get_change_customers_url);
            location_order_modal_employee_change_location_select.val('');
            setTimeout(function(){
                getPosibleResultsLocations(get_change_customers_url);
            }, 300);
            setTimeout(function(){
                change_position_location_button.show();
            }, 400);
        }
    });
    employees_work_day_select.change(function(){
        change_position_location_button.hide();
        if (employees_select.val() == '' || employees_work_day_select.val() == '' || employees_work_week_select.val() == ''){
            location_order_modal_employee_change_location_select.html();
        }else{
            location_order_modal_employee_change_location_select.val('');
            setTimeout(function(){
                getPosibleResultsLocations(get_change_customers_url);
            }, 300);
            setTimeout(function(){
                change_position_location_button.show();
            }, 400);
        }
    });
    // location_order_modal_employee_change_location_select.change(function(){
    //     if (employees_select.val() == '' || employees_work_day_select.val() == '' || location_order_modal_employee_change_location_select.val() == ''){
    //         change_position_location_button.hide();
    //     }else{
    //          change_position_location_button.show();
    //
    //     }
    // });

    function getPosibleResultsLocations($url) {
        $.ajax({
            dataType: "json",
            traditional: true,
            type: "GET",
            url: $url,
            data: {employees_select_id:employees_select.val(),employees_work_day_id:employees_work_day_select.val(),employees_work_week:employees_work_week_select.val()},
            success: function (data) {
                var message_code = data['message_code'];
                var _html_customers_select = data['_html_customers_select'];
                if (message_code == 1){
                    // console.log(_html);
                    location_order_modal_employee_change_location_select.html('');
                    location_order_modal_employee_change_location_select.append(_html_customers_select);
                    // $("#todo-task-modal").modal('hide');
                    // ajax_employee_map_div.append(_html);
                    // work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }

    function getDailyOptimization($url) {
        $.ajax({
            dataType: "json",
            traditional: true,
            type: "GET",
            url: $url,
            data: {},
            success: function (data) {
                var message_code = data['message_code'];
                if (message_code == 1){
                    // console.log(_html);
                    employeePlansShow();
                    // ajax_employee_map_div.append(_html);
                    // work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }

    function lockUnlock($url,$id) {
        $.ajax({
            dataType: "json",
            traditional: true,
            type: "GET",
            url: $url,
            data: {},
            success: function (data) {
                var message_code = data['message_code'];
                var is_lock = data['is_lock'];
                if (message_code == 1){
                    $('#plan-log-lock-button-'+$id).text(data['lock_button']);
                    if (is_lock == 1){
                        $('#plan-log-edit-button-'+$id).attr('disabled','disabled');
                    }else {
                        $('#plan-log-edit-button-'+$id).removeAttr('disabled');
                    }
                    // console.log(_html);
                    // ajax_employee_map_div.append(_html);
                    // work_plan_container.html(_html)
                }
            },
            error: function (data) {
                // work_plan_container.removeAttr('disabled');
            }
        });
    }

    function changePositionLocation() {
        if (employees_select.val() != '' && employees_work_day_select.val() != '') {
            // alert(location_order_modal_employee_change_location_select.val().length);
            work_plan_container.html("<div class='text-center'><img style='margin: 10px auto;' src='" + ajax_loader_url + "'/></div>");
            $.ajax({
                dataType: "json",
                traditional: true,
                type: "GET",
                url: change_position_location_button_url,
                data: {
                    employees_select_id: employees_select.val(),
                    employees_work_day_id: employees_work_day_select.val(),
                    employees_work_week:employees_work_week_select.val(),
                    chng_loc_ord_id: location_order_modal_employee_change_location_select.val()
                },
                success: function (data) {
                    var message_code = data['message_code'];
                    var _html_customers_select = data['_html_customers_select'];
                    if (message_code == 1) {
                        // console.log(_html);
                        location_order_modal_employee_change_location_select.html('');
                        location_order_modal_employee_change_location_select.append(_html_customers_select);
                        employeePlansShow();
                        // $("#todo-task-modal").modal('hide');
                        // ajax_employee_map_div.append(_html);
                        // work_plan_container.html(_html)
                    }
                },
                error: function (data) {
                    // work_plan_container.removeAttr('disabled');
                }
            });
        }
    }
